﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebShop.Models;

namespace WebShop.ViewModels
{
    public class ProductVM
    {
        public Product Product { get; set; }

        public List<Product> Products { get; set; }

        public Cart Cart { get; set; }

        public int Total { get; set; }
    }
}
