﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using WebApplication2.Models;

namespace WebShop.Models
{
    public class Cart
    {
        public int CartId { get; set; }

        public DateTime CartCreated { get; set; }

        public string ApplicationUserId { get; set; }

        [ForeignKey(nameof(ApplicationUserId))]
        public ApplicationUser ApplicationUser { get; set; }

        public List<ProductInCart> ProductsInCart { get; set; } = new List<ProductInCart>();

    }
}
