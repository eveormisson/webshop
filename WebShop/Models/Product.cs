﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebShop.Models
{
    public class Product
    {
        public int Id { get; set; }

        [MaxLength(128)]
        [Required]
        public string Image { get; set; }

        [MaxLength(128)]
        [Required]
        public string Productcode { get; set; }

        [MaxLength(128)]
        [Required]
        public string Name { get; set; }

        [MaxLength(1000)]
        [Required]
        public string Description { get; set; }

        [MaxLength(128)]
        [Required]
        public string Department { get; set; }

        public int Price { get; set; }

        [MaxLength(50)]
        [Required]
        public string Currency { get; set; }

        [MaxLength(128)]
        [Required]
        public string Store { get; set; }

        public bool Instock { get; set; }
    }
}
