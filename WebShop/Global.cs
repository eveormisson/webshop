﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebShop.Models;
using WebShop.Services.Interfaces;

namespace WebShop
{
    /// <summary>
    /// In this Global class the Shopping Cart of the logged in User is kept.
    /// A new Car is created upon every login.
    /// </summary>
    public static class Global
    {
        
        static Global() { }

        public static List<int> Cart { get; set; } 

        /// <summary>
        /// A method for adding product (id) to the list of products (id-s).
        /// </summary>
        /// <param name="id">Product id</param>
         public static void AddToCart(int id)
         {
            if (Cart == null) { Cart = new List<int>(); }
             Cart.Add(id);
         }

        /// <summary>
        /// A method for removing product (id) from the list of products (id-s).
        /// </summary>
        /// <param name="id">Product id</param>
        public static void RemoveFromCart(int id)
        {
            Cart.Remove(id);
        }
    }
}
