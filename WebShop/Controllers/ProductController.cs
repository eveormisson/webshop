﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApplication2.Data;
using WebShop.Models;
using WebShop.Services.Interfaces;
using WebShop.ViewModels;

namespace WebShop.Controllers
{
    public class ProductController : Controller
    {
        private readonly IProductService _productService;
        private readonly ICartService _cartService;
        

        public ProductController(IProductService productService, ICartService cartService)
        {
            _productService = productService;
            _cartService = cartService;
        }


        public async Task<IActionResult> Index(string search)
        {
            var vm = new ProductVM();
            var Products = await _productService.GetAllProductsAsync();

            // Search method for filtering products by store (country) name.
            if (!string.IsNullOrEmpty(search))
            {
                var searched = Products
                .Where(p => p.Store.ToUpper().Contains(search.ToUpper()));

                vm.Products = searched.ToList();
                return View(vm);
            }

            vm.Products = Products;
            return View(vm);
        }

        [Authorize]
        public async Task<IActionResult> Cart()
        {
            var vm = new ProductVM();
           
            // Getting the products from global Cart.
            vm.Products = await _cartService.ConvertedFromId(Global.Cart);

            // Calculating the total of the current Cart.
            vm.Total = _cartService.CartTotal(vm.Products);

            return View(vm);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var vm = new ProductVM();
            vm.Product = await _productService.GetProductByIdAsync(id.Value);


            if (vm.Product == null)
            {
                return NotFound();
            }

            return View(vm);
        }

        public async Task<IActionResult> InStock(string search)
        {
            var vm = new ProductVM();

            var Products = await _productService.GetAllInStockAsync();

            // Search method for filtering products by store (country) name.
            if (!string.IsNullOrEmpty(search))
            {
                var searched = Products
                .Where(p => p.Store.ToUpper().Contains(search.ToUpper()));

                vm.Products = searched.ToList();
                return View(vm);
            }

            vm.Products = Products;

            return View(vm);
        }


        [Authorize]
        public async Task<IActionResult> AddToCart(int? id)
        {

            var vm = new ProductVM();
            vm.Product = await _productService.GetProductByIdAsync(id.Value);

            if (vm.Product == null)
            {
                return NotFound();
            }

            // Adding the product to the global Cart.
            Global.AddToCart(id.Value);

            // Getting the products from the current global Cart.
            vm.Products = await _cartService.ConvertedFromId(Global.Cart);

            // Recalcultaing the total of the current Cart.
            vm.Total = _cartService.CartTotal(vm.Products);

            return View(vm);

        }

        [Authorize]
        public async Task<IActionResult> RemoveFromCart(int? id)
        {

            var vm = new ProductVM();
            vm.Product = await _productService.GetProductByIdAsync(id.Value);

            if (vm.Product == null)
            {
                return NotFound();
            }

            // Removing the product from the global Cart.
            Global.RemoveFromCart(id.Value);

            // Getting the products from the current global Cart.
            vm.Products = await _cartService.ConvertedFromId(Global.Cart);

            // Recalcultaing the total of the current Cart.
            vm.Total = _cartService.CartTotal(vm.Products);

            return View(vm);

        }
    }
}