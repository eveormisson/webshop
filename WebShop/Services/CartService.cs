﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication2.Data;
using WebShop.Models;
using WebShop.Services.Interfaces;

namespace WebShop.Services
{
    public class CartService : BaseService, ICartService
    {
        private IProductService _productService;
        
        public CartService(IProductService productService) : base()
        {
            _productService = productService;
        }

        public async Task<List<Product>> AddProductToCartAsync(int productId)
        {
            
            Global.AddToCart(productId);
            return await ConvertedFromId(Global.Cart);
            
        }

        public async Task<List<Product>> RemoveProductFromCartAsync(int productId)
        {
            Global.RemoveFromCart(productId);
            return await ConvertedFromId(Global.Cart);
        }

        public async Task<List<Product>> ConvertedFromId(List<int> IdList)
        {
            var list = new List<Product>();
        
            if (IdList == null)
            {
                IdList = new List<int>();
            }

            foreach(var id in IdList)
            {
                var item = await _productService.GetProductByIdAsync(id);
                list.Add(item);
            }

            return list;
        }

        public int CartTotal(List<Product> Cart)
        {
            var total = 0;

            foreach (var product in Cart)
            {
                var i = product.Price;
                total += i;
            }

            return total;
        }
    }
}
