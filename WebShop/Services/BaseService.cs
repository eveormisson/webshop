﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace WebShop.Services
{
    public class BaseService
    {
        protected HttpClient _client;

        public BaseService()
        {
            _client = new HttpClient();

            _client.BaseAddress = new Uri("https://erply-challenge.herokuapp.com/list");
            _client.DefaultRequestHeaders.Add("AUTH", "fae7b9f6-6363-45a1-a9c9-3def2dae206d");
            
        }

        /// <summary>
        /// An Generic method for getting a list of objects form a provided address.
        /// </summary>
        /// <typeparam name="T">The Model or Entity against what the method is used.</typeparam>
        /// <returns>Satus code 200 and the content if the request is successful or 404 (Not Found) if the request is not successful.</returns>
        protected async Task<T> GetAsync<T>()
        {
            var response = await _client.GetAsync(_client.BaseAddress);
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadAsAsync<T>();

        }

    }
}
