﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebShop.Models;

namespace WebShop.Services.Interfaces
{
    public interface ICartService
    {
        Task<List<Product>> AddProductToCartAsync(int productId);

        Task<List<Product>> RemoveProductFromCartAsync(int productId);

        Task<List<Product>> ConvertedFromId(List<int> IdList);

        int CartTotal(List<Product> Cart);
    }
}
