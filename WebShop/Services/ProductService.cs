﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebShop.Models;
using WebShop.Services.Interfaces;

namespace WebShop.Services
{
    public class ProductService : BaseService, IProductService
    {
        public ProductService() : base() { }

        /// <summary>
        /// A method for getting the list of products via the base method.
        /// </summary>
        /// <returns>Satus code 200 and the content if the request is successful or 404 (Not Found) if the request is not successful.</returns>
        public async Task<List<Product>> GetAllProductsAsync()
        {
            return await base.GetAsync<List<Product>>();
        }

        /// <summary>
        /// A method for getting a product by Id from the list of all products.
        /// </summary>
        /// <param name="id">Product id</param>
        /// <returns>The product item with the inserted id.</returns>
        public async Task<Product> GetProductByIdAsync(int id)
        {
            var list = await base.GetAsync<List<Product>>();

            var p = list.Where(a => a.Id == id).FirstOrDefault();

            if (p == null) return null;

            return p;
        }

        /// <summary>
        /// A method for filtering the products that are currently in stock from the list of all products.
        /// </summary>
        /// <returns>List of Products currently in stock</returns>
        public async Task<List<Product>> GetAllInStockAsync()
        {
            var list = await base.GetAsync<List<Product>>();

            var instock = list.Where(a => a.Instock == true).ToList();

            if (instock == null) return null;

            return instock;
        }
    }
}
