# README #

A siplme single page application for an e-shop.

### WebShop ###

* A siplme single page application for an e-shop.
* This application is created as a test work for Erply.
* Version 1.0

### Setup ###

* Download and open in Visual Studio.
* Press Ctrl F5 to start the application.
* The application takes test data from https://erply-challenge.herokuapp.com/list 
* with the AUTH key: fae7b9f6-6363-45a1-a9c9-3def2dae206d
* For now both the address and the key are hardcoded in the application.

### Contact ###

* Eve Ormisson
* eve.ormisson@artun.ee